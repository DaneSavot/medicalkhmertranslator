package com.dane.medicalkhmertranslator;

import java.util.List;

import com.dane.medicalkhmertranslator.adapter.BookmarkListAdapter;
import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.task.QueryContentLoader;
import com.dane.medicalkhmertranslator.task.QueryContentLoader.QueryMode;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class BookMarkFragment extends Fragment implements LoaderCallbacks<List<Content>> {
	private static final int GET_CONTENT = 0;
	View layout;
	public ListView subContentList;
	BookmarkListAdapter bookmarkListAdapter;
	MedicalTranslatorApp _app;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.setRetainInstance(true);
		_app = (MedicalTranslatorApp) getActivity().getApplicationContext();
		subContentList = (ListView) layout.findViewById(R.id.contentListView);
		bookmarkListAdapter = new BookmarkListAdapter(getActivity());
		subContentList.setAdapter( bookmarkListAdapter );
		getLoaderManager().initLoader(GET_CONTENT, null, this);
		
	}
	@Override
	public void onResume() {
		super.onResume();
		
		if (_app.isRefresh) {
			getLoaderManager().restartLoader(GET_CONTENT, null, this);
			_app.isRefresh = false;
		}
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		layout = inflater.inflate(R.layout.content_list, container, false);
		
		return layout;
	}

	@Override
	public Loader<List<Content>> onCreateLoader(int loaderID, Bundle bundle) {
		
		Loader<List<Content>> loader = null;
		
		switch (loaderID) {
		case GET_CONTENT:
			
			loader = new QueryContentLoader(getActivity(),QueryMode.BookmarkContent, null);
			
			break;

		default:
			break;
		}
			
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<List<Content>> arg0, List<Content> contents) {
		
		bookmarkListAdapter.clear();
		bookmarkListAdapter.addAll(contents);
		
	}
	public BookmarkListAdapter getBookmarkAdapter() {
		return bookmarkListAdapter;
	}
	@Override
	public void onLoaderReset(Loader<List<Content>> arg0) {
		
	}
	
}
