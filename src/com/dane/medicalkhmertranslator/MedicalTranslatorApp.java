package com.dane.medicalkhmertranslator;

import java.io.IOException;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dane.medicalkhmertranslator.db.MedicalKhmerDBHelper;
import com.dane.medicalkhmertranslator.db.dao.BookmarkDAO;
import com.dane.medicalkhmertranslator.db.dao.ContentDAO;

public class MedicalTranslatorApp extends Application {
	
	private static final String LOG_TAG = "MedicalTranslatorApp";
	
	private SQLiteDatabase db;
	private ContentDAO contentDAO;
	private BookmarkDAO bookmarkDAO;
	
	public boolean isRefresh = false;
	
	@Override
	public void onCreate() {
		
		super.onCreate();
		
		MedicalKhmerDBHelper dbHelper = new MedicalKhmerDBHelper(this);
		//db = dbHelper.getWritableDatabase();
		
		try {
			dbHelper.createDataBase();
			db = dbHelper.openDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(LOG_TAG, e.getMessage());
		}
		
		
		contentDAO = new ContentDAO(db);
		bookmarkDAO = new BookmarkDAO(db);
		
		Log.i(LOG_TAG, "Create Database File... DB Status: " + db.isOpen());
		
	}
	
	public ContentDAO getContentDAO(){
		
		return contentDAO;
		
	}
	
	public BookmarkDAO getBookmarkDAO() {
		return bookmarkDAO;
	}

}
