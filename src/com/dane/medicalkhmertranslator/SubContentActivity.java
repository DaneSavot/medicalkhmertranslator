package com.dane.medicalkhmertranslator;

import java.io.IOException;
import java.io.Serializable;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.db.model.SubMainContent;

public class SubContentActivity extends ActionBarActivity implements
		SubContentFragmentEventHandler, OnCompletionListener,
		OnAudioFocusChangeListener {
	private static final String CURRENT_POSITION = "current_position";
	private static final String CURRENT_CONTENT = "current_content";
	private static String LOG_TAG = "SubContentActivity";
	public static final String DATA = "data";
	boolean isPlaying = false;
	int position = -1;
	MediaPlayer currentMedia;
	AudioManager am;
	private Content mainContent;
	Content currentContent;
	MedicalTranslatorApp _app;

	// private ImageButton currentPlayButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
		_app = (MedicalTranslatorApp) this.getApplicationContext();

		setContentView(R.layout.activity_sub_main);
		Intent intent = getIntent();
		mainContent = (Content) intent.getExtras().get(DATA);
		this.setTitle(mainContent.getValue());
		
		currentMedia = (MediaPlayer) getLastCustomNonConfigurationInstance();
		if (currentMedia == null) {
			currentMedia = new MediaPlayer();
		}
		if (savedInstanceState != null) {
			currentContent = (Content) savedInstanceState
					.getSerializable(CURRENT_CONTENT);
			position = savedInstanceState.getInt(CURRENT_POSITION);
			return;
		}


		SubContentFragment subContent = new SubContentFragment();

		Bundle args = new Bundle();
		args.putInt(SubContentFragment.PARENT_ID, mainContent.getID());
		
		subContent.setArguments(args);

		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();

		ft.replace(R.id.sub_main, subContent, SubContentFragment.FRAGMENT_TAG);
		ft.commit();

	}

	@Override
	public void onPlayButtonClick(View v, Content content, int position) {

		if (this.position != position) {
			stopAudio();
			playAudio(v, content, position);
		} else {
			if (content.get_play_icon() == R.drawable.ic_play)
				playAudio(v, content, position);
			else
				stopAudio();
		}
		/*
		 * else { boolean isNewPlayButton = !v.equals(currentPlayButton);
		 * stopAudio(); if (isNewPlayButton) { playAudio(v, content, position);
		 * } }
		 */

	}

	private void stopAudio() {
		// currentPlayButton.setImageResource(R.drawable.ic_play);
		if (currentMedia != null && currentContent != null) {
			currentMedia.stop();
			onCompletion(currentMedia);
		}
	}

	private void playAudio(View v, Content content, int position) {
		int result;

		this.currentContent = content;
		this.position = position;

		result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);

		if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {

			try {
				AssetFileDescriptor afd;
				afd = getAssets().openFd(content.getVoice() + ".mp3");
				currentMedia.reset();
				currentMedia.setDataSource(afd.getFileDescriptor(),
						afd.getStartOffset(), afd.getLength());
				currentMedia.prepare();
				currentMedia.setOnCompletionListener(this);
				currentMedia.start();
				ImageButton currentPlayButton = (ImageButton) v;
				content.set_play_icon(R.drawable.ic_stop);
				currentPlayButton.setImageResource(content.get_play_icon());
			} catch (IOException e) {
				Log.e(LOG_TAG, e.getMessage());
			}

			/*
			 * currentMedia = MediaPlayer.create(this, R.raw.morning);
			 * currentMedia.setOnCompletionListener(this); currentMedia.start();
			 */

		}
	}

	@Override
	public void onBookmarkButtonClick(View v, Content content) {

		if (content.get_is_bookmark() == false) {

			long rowID = _app.getBookmarkDAO().bookmark(content.getID());
			if (rowID > 0) {
				content.set_is_bookmark(true);
				Toast.makeText(this, "Bookmarked.", Toast.LENGTH_SHORT).show();
			}	
		}
		else {
			long rowAffected = _app.getBookmarkDAO().unBookmark(content.getID());
			if (rowAffected >0) {
				content.set_is_bookmark(false);
			}
		}
		
		_app.isRefresh = true;
		
		ImageButton bookmarkButton = (ImageButton) v;
		bookmarkButton.setImageResource(content.get_bookmark_icon());

	}

	@Override
	public void onCompletion(MediaPlayer mp) {

		currentContent.set_play_icon(R.drawable.ic_play);
		// currentPlayButton.setImageResource(currentContent.get_play_icon());

		SubContentFragment subContent = (SubContentFragment) getSupportFragmentManager()
				.findFragmentByTag(SubContentFragment.FRAGMENT_TAG);
		if (subContent != null) {
			View listItem = subContent.subContentList.getChildAt(position
					- subContent.subContentList.getFirstVisiblePosition());
			if (listItem != null) {
				ImageButton playButton = (ImageButton) listItem
						.findViewById(R.id.playIcon);
				playButton.setImageResource(currentContent.get_play_icon());
			}
		}

		// mp.release();
		// currentMedia = mp = null;
		// currentPlayButton = null;

		am.abandonAudioFocus(this);

	}

	@Override
	public void onAudioFocusChange(int focusChange) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Content content = (Content) parent.getItemAtPosition(position);
		if (content instanceof SubMainContent) {
			Intent intent = new Intent(this, SubContentActivity.class);
			intent.putExtra(SubContentActivity.DATA, (Serializable) content);

			startActivity(intent);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		outState.putSerializable(CURRENT_CONTENT, currentContent);
		outState.putInt(CURRENT_POSITION, position);

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		currentMedia.release();
		currentMedia = null;
		super.onBackPressed();
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		MediaPlayer mediaPlayer = currentMedia;
		// Clear our member variable to guarantee this Activity
		// is allowed to GC after onDestroy()
		currentMedia = null;
		return mediaPlayer;
	}

}
