package com.dane.medicalkhmertranslator;

import java.util.List;

import com.dane.medicalkhmertranslator.adapter.ContentListAdapter;
import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.task.QueryContentLoader;
import com.dane.medicalkhmertranslator.task.QueryContentLoader.QueryMode;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ContentFragment extends Fragment implements LoaderCallbacks<List<Content>>, OnItemClickListener{
	private static final int GET_CONTENT = 0;
	View layout;
	ContentFragmentEventHandler handler; 
	ContentListAdapter contentListAdapter;
	ListView contentListView;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.setRetainInstance(true);
		handler = (ContentFragmentEventHandler) getActivity();
		contentListAdapter = new ContentListAdapter(getActivity());
		
		contentListView = (ListView) layout.findViewById(R.id.contentListView);
		
		contentListView.setAdapter(contentListAdapter);
		contentListView.setOnItemClickListener(this);
		
		getLoaderManager().initLoader(GET_CONTENT, null, this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		layout = inflater.inflate(R.layout.content_list, container, false);
		
		return layout;
	}

	@Override
	public Loader<List<Content>> onCreateLoader(int loaderID, Bundle bundle) {
		
		Loader<List<Content>> loader = null;
		
		switch (loaderID) {
		case GET_CONTENT:
			
			loader = new QueryContentLoader(getActivity(),QueryMode.MainContent, bundle);
			
			break;

		default:
			break;
		}
			
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<List<Content>> arg0, List<Content> contents) {
		
		contentListAdapter.clear();
		contentListAdapter.addAll(contents);

		handler.onLoaderFinish(contents);
	}

	@Override
	public void onLoaderReset(Loader<List<Content>> arg0) {
		
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		
		
		handler.onItemClick(parent, view, position, id);
		
	}



}
