package com.dane.medicalkhmertranslator.db.dao;

import java.util.List;

public interface Dao<T> {
	
	long save(T type);
	T get(int id);
	List<T> getAllMainContent();
	List<T> getAllChildContent(int parentID);
	
}
