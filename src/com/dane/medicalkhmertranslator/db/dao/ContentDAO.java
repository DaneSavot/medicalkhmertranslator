package com.dane.medicalkhmertranslator.db.dao;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.db.model.ContentFactory;
import com.dane.medicalkhmertranslator.db.model.ContentTable;
import com.dane.medicalkhmertranslator.db.model.ContentTable.ContentColumn;

public class ContentDAO implements Dao<Content> {

	private SQLiteDatabase db;

	public ContentDAO(SQLiteDatabase db) {
		this.db = db;
	}

	@Override
	public long save(Content type) {

		return 0;
	}

	@Override
	public Content get(int id) {

		Content content = null;

		Cursor c = db.query(ContentTable.TABLE_NAME, null, ContentColumn._ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null);
		if (c.moveToFirst()){
			content = bunldContentFromCursor(c);
		}
		
		if (!c.isClosed()) {
			c.close();
		}
		
		return content;
	}
	
	@Override
	public List<Content> getAllMainContent() {
		
		List<Content> contents = new ArrayList<Content>();
		
		//Cursor c = db.query(ContentTable.TABLE_NAME, null, ContentColumn.TYPE + "=0", null, null, null, null);
		
		Cursor c = db.rawQuery("SELECT contents._id, value, voice, type, parent, valueInOtherLang from contents  join languageContents  on contents._id = languageContents.contentid where type = 0",null);
		if (c.moveToFirst()){
			
			do {
				
				contents.add(bunldContentFromCursor(c));
				
			} while (c.moveToNext());
			
		}
		
		if (!c.isClosed()) {
			c.close();
		}
		
		return contents;
	}
	
	@Override
	public List<Content> getAllChildContent(int parentID) {
		List<Content> contents = new ArrayList<Content>();
		
		//Cursor c = db.query(ContentTable.TABLE_NAME, null, ContentColumn.PARENT + "=?", new String[]{parentID+""} , null, null, null);
		Cursor c = db.rawQuery("SELECT contents._id, value, voice, type, parent, valueInOtherLang, (select Bookmarks._id from Bookmarks where Bookmarks.contentID=contents._id ) as bookmarkID from contents  join languageContents  on contents._id = languageContents.contentid where parent =?", new String[]{parentID+""});
		if (c.moveToFirst()){
			
			do {
				
				contents.add(bunldContentFromCursor(c));
				
			} while (c.moveToNext());
			
		}
		
		if (!c.isClosed()) {
			c.close();
		}
		
		return contents;
	}
	
	private Content bunldContentFromCursor(Cursor c) {
		
		Content content = null;
		
		content = ContentFactory.buildContent(c);
		
		return content;
	}


}
