package com.dane.medicalkhmertranslator.db.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.db.model.ContentFactory;

public class BookmarkDAO {

	private SQLiteDatabase db;

	public BookmarkDAO(SQLiteDatabase db) {
		this.db = db;
	}

	public long bookmark(int contentID) {
		ContentValues values = new ContentValues();
		values.put("contentID", contentID);
		return db.insert("Bookmarks", null, values);
	}

	public long unBookmark(int contentID) {
		ContentValues values = new ContentValues();
		values.put("contentID", contentID);
		return db.delete("Bookmarks", "contentID=?",
				new String[] { String.valueOf(contentID) });
	}

	public List<Content> getAllContents() {
		List<Content> contents = new ArrayList<Content>();

		// Cursor c = db.query(ContentTable.TABLE_NAME, null,
		// ContentColumn.PARENT + "=?", new String[]{parentID+""} , null, null,
		// null);
		Cursor c = db
				.rawQuery(
						"SELECT contents._id, value, voice, type, parent, valueInOtherLang from (bookmarks join contents on bookmarks.contentID = contents._id ) join  languageContents  on bookmarks.contentID = languageContents.contentid",
						null);
		if (c.moveToFirst()) {

			do {

				contents.add(bunldContentFromCursor(c));

			} while (c.moveToNext());

		}

		if (!c.isClosed()) {
			c.close();
		}

		return contents;
	}

	private Content bunldContentFromCursor(Cursor c) {

		Content content = null;

		content = ContentFactory.buildContent(c);

		return content;
	}

}
