package com.dane.medicalkhmertranslator.db.model;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

public final class ContentTable {
	
	public static final String LOG_TAG = "ContentTable";
	public static final String TABLE_NAME = "Contents";
	
	public static abstract class ContentColumn implements BaseColumns {
		
		public static final String VALUE = "value";
		public static final String TYPE = "type";
		public static final String VOICE = "voice";
		public static final String PARENT = "parent";
		public static final String CREATEDDATE = "createdDate";
				
	}
	
	public static void onCreate(SQLiteDatabase db) {
		String sql;
		sql = "CREATE TABLE " + TABLE_NAME + " ( " + 
			BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
			ContentColumn.VALUE + " TEXT, " +
			ContentColumn.VOICE + " TEXT," +
			ContentColumn.TYPE + " INTEGER, " + 
			ContentColumn.CREATEDDATE + " TEXT, " +
			ContentColumn.PARENT + " INTEGER );";
		
		db.execSQL(sql);
		
		Log.i(LOG_TAG, "Create Contents table.");
		
	}
	
	public static void onUpgrade(SQLiteDatabase db) {
		String sql; 
		sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
		db.execSQL(sql);
		onCreate(db);
		Log.i(LOG_TAG, "Recreat Contents table.");
	}

}
