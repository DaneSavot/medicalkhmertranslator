package com.dane.medicalkhmertranslator.db.model;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

public final class LanguageContentTable {
	
	public static final String LOG_TAG = "LanguageContentTable";
	public static final String TABLE_NAME = "languageContents";
	
	public static abstract class LanguageContentColumn implements BaseColumns {
		
		public static final String CONTENT_ID = "contentID";
		public static final String VALUE_IN_OTHER_LANG = "valueInOtherLang";
		public static final String LANGUAGE = "language";
				
	}
	
	public static void onCreate(SQLiteDatabase db) {
		
		String sql; 
		sql = "CREATE TABLE " + TABLE_NAME + " ( " + 
			BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
			LanguageContentColumn.CONTENT_ID + " INTEGER, " + 
			LanguageContentColumn.VALUE_IN_OTHER_LANG + " TEXT, " + 
			LanguageContentColumn.LANGUAGE + " TEXT )";
		db.execSQL(sql);
		Log.i(LOG_TAG, "Create LanguageContent table.");
		
	}
	
	public static void onUpgrade(SQLiteDatabase db) {
		
		String sql; 
		
		sql = "DROP TABLE IF EXISTS " + TABLE_NAME; 
		
		db.execSQL(sql);
		onCreate(db);
		Log.i(LOG_TAG, "Recreate LanguageContent table.");
		
	}
	

}
