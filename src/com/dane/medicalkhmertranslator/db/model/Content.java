package com.dane.medicalkhmertranslator.db.model;

import java.io.Serializable;

import com.dane.medicalkhmertranslator.R;
import com.dane.medicalkhmertranslator.db.model.ContentTable.ContentColumn;
import com.dane.medicalkhmertranslator.db.model.LanguageContentTable.LanguageContentColumn;

import android.database.Cursor;

@SuppressWarnings("serial")
public abstract class Content implements Serializable {
	
	private int _id; 
	private String _value;
	private int _type;
	private String _voice;
	private int _parent;
	private String _valueInOtherLanguage;
	private int _play_icon = R.drawable.ic_play;
	private Boolean _is_bookmark = false;
	
	public Content(Cursor c) {
		if (c!= null) {
			setID(c.getInt(c.getColumnIndex(ContentColumn._ID)));
			setValue(c.getString(c.getColumnIndex(ContentColumn.VALUE)));
			setType(c.getInt(c.getColumnIndex(ContentColumn.TYPE)));
			setParent(c.getInt(c.getColumnIndex(ContentColumn.PARENT)));
			setVoice(c.getString(c.getColumnIndex(ContentColumn.VOICE)));
			setValueInOtherLanguage(c.getString(c.getColumnIndex(LanguageContentColumn.VALUE_IN_OTHER_LANG)));
			
			int columnIndex = c.getColumnIndex("bookmarkID");
			if (columnIndex>=0 && c.getInt(columnIndex) > 0 ) {
				_is_bookmark = true;	
			}
			
		}
	}

	public int getID() {
		return _id;
	}
	
	public void setID(int id) {
		this._id = id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		this._value = value;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		this._type = type;
	}

	public int getParent() {
		return _parent;
	}

	public void setParent(int parent) {
		this._parent = parent;
	}

	public String getValueInOtherLanguage() {
		return _valueInOtherLanguage;
	}

	public void setValueInOtherLanguage(String _language) {
		this._valueInOtherLanguage = _language;
	}

	public String getVoice() {
		return _voice;
	}

	public void setVoice(String _voice) {
		this._voice = _voice;
	}

	public int get_play_icon() {
		return _play_icon;
	}

	public void set_play_icon(int _play_icon) {
		this._play_icon = _play_icon;
	}

	public Boolean get_is_bookmark() {
		return _is_bookmark;
	}

	public void set_is_bookmark(Boolean _is_bookmark) {
		this._is_bookmark = _is_bookmark;
	}
	
	public int get_bookmark_icon() {
		if (_is_bookmark ) {
			return R.drawable.ic_bookmarked;
		}
		else {
			return R.drawable.ic_bookmark;
		}
				
			
	}	

}
