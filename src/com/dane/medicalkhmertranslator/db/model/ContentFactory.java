package com.dane.medicalkhmertranslator.db.model;

import com.dane.medicalkhmertranslator.db.model.ContentTable.ContentColumn;

import android.database.Cursor;

public class ContentFactory {
	
	public static final int MAIN_CONTENT = 0;
	public static final int SUB_MAIN_CONTENT = 1;
	public static final int QUESTION = 2;
	
	public static Content buildContent(Cursor c) {
		Content content = null;
		if (c != null) {
			
			int type = c.getInt(c.getColumnIndex(ContentColumn.TYPE));
			switch (type) {
			case MAIN_CONTENT:
				content = new MainContent(c);
				break;
			case SUB_MAIN_CONTENT: 
				content = new SubMainContent(c);
				break;
			case QUESTION: 
				content = new Question(c);
				break;
			default:
				break;
			}
		}
		return content;
	}

}
