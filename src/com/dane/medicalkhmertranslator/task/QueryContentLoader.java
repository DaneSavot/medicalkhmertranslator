package com.dane.medicalkhmertranslator.task;

import java.util.List;

import com.dane.medicalkhmertranslator.MedicalTranslatorApp;
import com.dane.medicalkhmertranslator.db.model.Content;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
//http://www.androiddesignpatterns.com/2012/08/implementing-loaders.html?m=1
public class QueryContentLoader extends AsyncTaskLoader<List<Content>> {

	public enum QueryMode {
		MainContent, SubContent, BookmarkContent
	}

	private final String PARENT_ID = "parent_id";

	List<Content> _contents = null;
	MedicalTranslatorApp _app;
	QueryMode _queryMode;
	Bundle _args;

	public QueryContentLoader(Context context, QueryMode queryMode, Bundle args) {
		super(context);
		_app = (MedicalTranslatorApp) context.getApplicationContext();
		this._queryMode = queryMode;
		this._args = args;
	}

	@Override
	protected void onStartLoading() {

		if (_contents != null) {
			// Deliver any previously loaded data immediately.
			deliverResult(_contents);
		}

		/*
		 * // Begin monitoring the underlying data source. if (mObserver ==
		 * null) { mObserver = new SampleObserver(); // TODO: register the
		 * observer }
		 */

		if (_contents == null) {
			forceLoad();
		}

	}

	/**
	 * This is where the bulk of our work is done. This function is called in a
	 * background thread and should generate a new set of data to be published
	 * by the loader.
	 */
	@Override
	public List<Content> loadInBackground() {

		switch (_queryMode) {
		case MainContent:
			_contents = _app.getContentDAO().getAllMainContent();

			// Content content1;
			/*
			 * content1 = new MainContent(); content1.setType(0);
			 * content1.setValue("Introduction");
			 * content1.set_language("Sachkdey Chap Phderm");
			 * _contents.add(content1);
			 * 
			 * content1 = new MainContent(); content1.setType(0);
			 * content1.setValue("Numbers"); content1.set_language("Lek");
			 * _contents.add(content1);
			 * 
			 * content1 = new MainContent(); content1.setType(0);
			 * content1.setValue("Commands"); content1.set_language("Commands");
			 * _contents.add(content1);
			 * 
			 * content1 = new MainContent(); content1.setType(0);
			 * content1.setValue("Mental Health");
			 * content1.set_language("Sokhaphea Plov Chet");
			 * _contents.add(content1);
			 */
			break;
		case SubContent:
			_contents = _app.getContentDAO().getAllChildContent(
					_args.getInt(PARENT_ID));
			/*
			 * content1 = new Question(); content1.setType(2);
			 * content1.setValue("Hello."); content1.set_language("Sou Sdey.");
			 * _contents.add(content1);
			 * 
			 * content1 = new Question(); content1.setType(2);
			 * content1.setValue("Good morning.");
			 * content1.set_language("Prolem Sou Sdey.");
			 * _contents.add(content1);
			 * 
			 * content1 = new Question(); content1.setType(2);
			 * content1.setValue("Good afternoon.");
			 * content1.set_language("Sayon Sou Sdey.");
			 * _contents.add(content1);
			 * 
			 * content1 = new Question(); content1.setType(2);
			 * content1.setValue("Good evening.");
			 * content1.set_language("Reatrey Sou Sdey.");
			 * _contents.add(content1);
			 */

			break;
		case BookmarkContent:

			_contents = _app.getBookmarkDAO().getAllContents();

			break;
		default:
			break;
		}

		return _contents;

	}

	/********************************************************/
	/** (2) Deliver the results to the registered listener **/
	/********************************************************/

	@Override
	public void deliverResult(List<Content> data) {
		if (isReset()) {
			// The Loader has been reset; ignore the result and invalidate the
			// data.
            if (data != null) {
    			// releaseResources(data);
            }

			return;
		}

		// Hold a reference to the old data so it doesn't get garbage collected.
		// We must protect it until the new data has been delivered.
		List<Content> oldData = _contents;
		_contents = data;

		if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
			super.deliverResult(data);
		}

		// Invalidate the old data as we don't need it any more.
		if (oldData != null && oldData != data) {
			// releaseResources(oldData);
		}
	}

	@Override
	protected void onStopLoading() {
		// The Loader is in a stopped state, so we should attempt to cancel the
		// current load (if there is one).
		cancelLoad();

		// Note that we leave the observer as is. Loaders in a stopped state
		// should still monitor the data source for changes so that the Loader
		// will know to force a new load if it is ever started again.
	}

	@Override
	protected void onReset() {
		// Ensure the loader has been stopped.
		onStopLoading();

		// At this point we can release the resources associated with 'mData'.
		if (_contents != null) {
			//releaseResources(_contents);
			_contents = null;
		}

/*		// The Loader is being reset, so we should stop monitoring for changes.
		if (mObserver != null) {
			// TODO: unregister the observer
			mObserver = null;
		}*/
	}

	@Override
	public void onCanceled(List<Content> data) {
		// Attempt to cancel the current asynchronous load.
		super.onCanceled(data);

		// The load has been canceled, so we should release the resources
		// associated with 'data'.
		//releaseResources(data);
	}

	/**
	 * Helper function to take care of releasing resources associated with an
	 * actively loaded data set.
	 */

	/*
	 * private void releaseResource() { // For a simple List<> there is nothing
	 * to do. For something // like a Cursor, we would close it here.
	 * 
	 * }
	 */

}
