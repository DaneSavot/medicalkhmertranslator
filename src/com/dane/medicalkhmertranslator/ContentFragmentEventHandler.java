package com.dane.medicalkhmertranslator;

import java.util.List;

import com.dane.medicalkhmertranslator.db.model.Content;

import android.view.View;
import android.widget.AdapterView;

public interface ContentFragmentEventHandler {
	
	public void onItemClick(AdapterView<?> parent, View view, int position, long id);
	public void onLoaderFinish(List<Content> contents);
}
