package com.dane.medicalkhmertranslator;

import java.util.List;

import com.dane.medicalkhmertranslator.adapter.ContentListAdapter;
import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.task.QueryContentLoader;
import com.dane.medicalkhmertranslator.task.QueryContentLoader.QueryMode;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SubContentFragment extends Fragment implements LoaderCallbacks<List<Content>>, OnItemClickListener{
	
	public static final String FRAGMENT_TAG = "subContent";
	public static final String PARENT_ID = "parent_id";
	private static final int GET_CONTENT = 0; 
	private int parentID;
	View layout;
	public ListView subContentList;
	ContentListAdapter contentListAdapter;
	SubContentFragmentEventHandler handler;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		handler = (SubContentFragmentEventHandler) getActivity();
		parentID = getArguments().getInt(PARENT_ID);
		this.setRetainInstance(true);
		Bundle args = new Bundle();
		args.putInt(PARENT_ID, parentID);
		
		subContentList = (ListView) layout.findViewById(R.id.contentListView);
		contentListAdapter = new ContentListAdapter(getActivity());
		subContentList.setAdapter( contentListAdapter );
		subContentList.setOnItemClickListener(this);
		
		getLoaderManager().initLoader(GET_CONTENT, args, this);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		layout = inflater.inflate(R.layout.content_list, container, false);
		
		return layout;
	}

	@Override
	public Loader<List<Content>> onCreateLoader(int loaderID, Bundle args) {
		
		Loader<List<Content>> loader = null;
		
		switch (loaderID) {
		case GET_CONTENT:
			
			loader = new QueryContentLoader(getActivity(), QueryMode.SubContent, args);
			
			break;

		default:
			break;
		}
		
		
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<List<Content>> arg0, List<Content> contents) {
		
		contentListAdapter.clear();
		contentListAdapter.addAll(contents);

		
	}

	@Override
	public void onLoaderReset(Loader<List<Content>> arg0) {
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		handler.onItemClick(parent, view, position, id);
		
	}

}
