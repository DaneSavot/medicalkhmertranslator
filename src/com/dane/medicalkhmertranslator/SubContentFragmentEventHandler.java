package com.dane.medicalkhmertranslator;


import com.dane.medicalkhmertranslator.db.model.Content;

import android.view.View;
import android.widget.AdapterView;

public interface SubContentFragmentEventHandler {
	public void onPlayButtonClick(View v, Content content, int position);
	public void onBookmarkButtonClick(View v, Content content);
	public void onItemClick(AdapterView<?> parent, View view, int position, long id);
}
