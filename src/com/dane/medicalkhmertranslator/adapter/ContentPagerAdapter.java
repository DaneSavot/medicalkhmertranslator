/**
 * 
 */
package com.dane.medicalkhmertranslator.adapter;


import com.dane.medicalkhmertranslator.BookMarkFragment;
import com.dane.medicalkhmertranslator.ContentFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * @author Dane
 *
 */
public class ContentPagerAdapter extends FragmentStatePagerAdapter {
	
	//private List<Fragment> fragments;
	
	private SparseArray<Fragment> fragments;
	
	public static final String[] tabs = {"Content", "Bookmark"};

	public ContentPagerAdapter(FragmentManager fm) {
		super(fm);
		fragments = new SparseArray<Fragment>();
	}
	
/*	public void addItem(Fragment fragment) {
		fragments.add(fragment);
	}*/

	@Override
	public Fragment getItem(int position) {
		
		//return fragments.get(position);
		switch (position) {
		case 0:
			ContentFragment contentFragment = new ContentFragment();
						return contentFragment;
		default:
			BookMarkFragment bookmarkFragment = new BookMarkFragment();
			return bookmarkFragment;
		}
		
	}
	
	public Fragment getFragment(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return tabs.length;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
			return tabs[position];
	}

	public static String makeFragmentName(int viewId, int index) {
	     return "android:switcher:" + viewId + ":" + index;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Fragment instantiateItem = (Fragment) super.instantiateItem(container, position);
		fragments.put(position, instantiateItem);
		return instantiateItem;
	}
	
}
