package com.dane.medicalkhmertranslator.adapter;

import com.dane.medicalkhmertranslator.R;
import com.dane.medicalkhmertranslator.SubContentFragmentEventHandler;
import com.dane.medicalkhmertranslator.db.model.Content;
import com.dane.medicalkhmertranslator.db.model.MainContent;
import com.dane.medicalkhmertranslator.db.model.SubMainContent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContentListAdapter extends ArrayAdapter<Content> implements OnClickListener {
	
	static class ViewHolder {
		
		ImageView contentIcon;
		TextView contentTitle;
		TextView contentLanguage;
		
		ImageView playIcon;
		TextView questionTitle;
		TextView questionLanguage;
		ImageView bookmarkIcon;
		
		int position;
		
	}
	
	
	public ContentListAdapter(Context context) {
		super(context, R.layout.content_list);
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Content content = getItem(position);
		if (content instanceof MainContent || content instanceof SubMainContent) {
			if (convertView == null) {

				LayoutInflater inflater = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				convertView = inflater.inflate(R.layout.content_subitem,
						parent, false);

				ViewHolder viewHolder = new ViewHolder();

				viewHolder.contentIcon = (ImageView) convertView
						.findViewById(R.id.contentIcon);
				viewHolder.contentTitle = (TextView) convertView
						.findViewById(R.id.contentTitle);
				viewHolder.contentLanguage = (TextView) convertView
						.findViewById(R.id.contentLanguage);

				convertView.setTag(viewHolder);

			}

			ViewHolder viewHolder = (ViewHolder) convertView.getTag();
			viewHolder.position = position;
			viewHolder.contentTitle.setText(content.getValue());
			viewHolder.contentLanguage.setText(content.getValueInOtherLanguage());
			return convertView;
		}
		else {
			
			if (convertView == null) {

				LayoutInflater inflater = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				convertView = inflater.inflate(R.layout.content_question_item,
						parent, false);

				ViewHolder viewHolder = new ViewHolder();

				viewHolder.playIcon = (ImageView) convertView
						.findViewById(R.id.playIcon);
				
				viewHolder.playIcon.setOnClickListener(this);
				
				viewHolder.questionTitle = (TextView) convertView
						.findViewById(R.id.questionTitle);
				
				viewHolder.questionLanguage = (TextView) convertView
						.findViewById(R.id.questionLanguage);
				
				viewHolder.bookmarkIcon = (ImageView) convertView.findViewById(R.id.bookmarkIcon);
				viewHolder.bookmarkIcon.setOnClickListener(this);
				
				convertView.setTag(viewHolder);

			}

			ViewHolder viewHolder = (ViewHolder) convertView.getTag();
			viewHolder.playIcon.setImageResource(content.get_play_icon());
			viewHolder.bookmarkIcon.setImageResource(content.get_bookmark_icon());
			viewHolder.questionTitle.setText(content.getValue());
			viewHolder.questionLanguage.setText(content.getValueInOtherLanguage());
			viewHolder.position = position;
			return convertView;
			
		}
	}
	
	@Override
	public void onClick(View v) {
	
/*		position = subContentList.getPositionForView((View) v.getParent());
		Content content = (Content) subContentList.getAdapter().getItem(position);*/
		int position;
		SubContentFragmentEventHandler handler;
			
		ViewHolder viewHolder = (ViewHolder) ((View)v.getParent()).getTag();
		

		
		switch (v.getId()) {
		case R.id.playIcon:
			handler = (SubContentFragmentEventHandler) getContext();
			position = viewHolder.position;
			handler.onPlayButtonClick(v, getItem(position), position);
			break;
		case R.id.bookmarkIcon:
			handler = (SubContentFragmentEventHandler) getContext();
			position = viewHolder.position;
			handler.onBookmarkButtonClick(v, getItem(position));
			break;
		default:
			break;
		}
		
		
	}
}
