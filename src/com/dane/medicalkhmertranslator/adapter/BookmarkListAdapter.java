package com.dane.medicalkhmertranslator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dane.medicalkhmertranslator.BookmarkFragmentEventHandler;
import com.dane.medicalkhmertranslator.R;
import com.dane.medicalkhmertranslator.db.model.Content;

public class BookmarkListAdapter extends ArrayAdapter<Content> implements OnClickListener {
	
	static class ViewHolder {
		
		ImageView playIcon;
		TextView questionTitle;
		TextView questionLanguage;
		ImageView deleteIcon;
		int position;
		
	}
	
	
	public BookmarkListAdapter(Context context) {
		super(context, R.layout.content_list);
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Content content = getItem(position);
		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.bookmark_item,
					parent, false);

			ViewHolder viewHolder = new ViewHolder();

			viewHolder.playIcon = (ImageView) convertView
					.findViewById(R.id.playIcon);
			viewHolder.playIcon.setOnClickListener(this);
			
			viewHolder.questionTitle = (TextView) convertView
					.findViewById(R.id.questionTitle);
			viewHolder.questionLanguage = (TextView) convertView
					.findViewById(R.id.questionLanguage);
			viewHolder.deleteIcon = (ImageView) convertView.findViewById(R.id.deleteIcon);
			viewHolder.deleteIcon.setOnClickListener(this);
			convertView.setTag(viewHolder);

		}

		ViewHolder viewHolder = (ViewHolder) convertView.getTag();
		viewHolder.playIcon.setImageResource(content.get_play_icon());
		viewHolder.questionTitle.setText(content.getValue());
		viewHolder.questionLanguage.setText(content.getValueInOtherLanguage());
		viewHolder.position = position;
		return convertView;
	}

	@Override
	public void onClick(View v) {
		int position;
		BookmarkFragmentEventHandler handler;
			
		ViewHolder viewHolder = (ViewHolder) ((View)v.getParent()).getTag();
		
		switch (v.getId()) {
		case R.id.playIcon:
			handler = (BookmarkFragmentEventHandler) getContext();
			position = viewHolder.position;
			handler.onPlayButtonClick(v, getItem(position), position);
			break;
		case R.id.deleteIcon:
			handler = (BookmarkFragmentEventHandler) getContext();
			position = viewHolder.position;
			handler.onDeleteButtonClick(v, getItem(position),position);
			break;
		default:
			break;
		}		
	}
	

}
