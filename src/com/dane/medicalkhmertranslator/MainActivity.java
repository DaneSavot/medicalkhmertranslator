package com.dane.medicalkhmertranslator;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import com.dane.medicalkhmertranslator.adapter.ContentPagerAdapter;
import com.dane.medicalkhmertranslator.db.model.Content;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;

public class MainActivity extends ActionBarActivity implements
		ContentFragmentEventHandler, BookmarkFragmentEventHandler,
		OnCompletionListener, OnAudioFocusChangeListener {

	private static String LOG_TAG = "MainActivity";
	private static final String CURRENT_POSITION = "current_position";
	private static final String CURRENT_CONTENT = "current_content";

	ViewPager mViewPager;
	ActionBar mActionBar;
	List<Content> mainContents;
	int position = -1;
	MediaPlayer currentMedia;
	AudioManager am;
	Content currentContent;
	MedicalTranslatorApp _app;

	ContentPagerAdapter contentPagerAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
		_app = (MedicalTranslatorApp) this.getApplicationContext();

		setContentView(R.layout.activity_main);

		mActionBar = getSupportActionBar();
		mViewPager = (ViewPager) findViewById(R.id.pager);

		createActionBarTabs();
		setupContentPager();


		
		currentMedia = (MediaPlayer) getLastCustomNonConfigurationInstance();
		if (currentMedia == null) {
			currentMedia = new MediaPlayer();
		}
		if (savedInstanceState != null) {
			currentContent = (Content) savedInstanceState
					.getSerializable(CURRENT_CONTENT);
			position = savedInstanceState.getInt(CURRENT_POSITION);
			return;
		}
		


		/*
		 * FragmentTransaction transaction = fm.beginTransaction();
		 * transaction.add(R.id.pager, new ContentFragment(),
		 * "ContentFragment"); transaction.commit();
		 */

	}

	private void setupContentPager() {
		FragmentManager fm = getSupportFragmentManager();
		contentPagerAdapter = new ContentPagerAdapter(fm);
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

					// Fire when user swipes the page
					@Override
					public void onPageSelected(int position) {
						// When swiping between pages, select the
						// corresponding tab.
						mActionBar.setSelectedNavigationItem(position);

					}

				});
		mViewPager.setAdapter(contentPagerAdapter);
		
	}

	private void createActionBarTabs() {

		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (int i = 0; i < ContentPagerAdapter.tabs.length; i++) {
			mActionBar.addTab(mActionBar.newTab()
					.setText(ContentPagerAdapter.tabs[i])
					.setTabListener(new TabListener() {

						@Override
						public void onTabUnselected(Tab arg0,
								FragmentTransaction arg1) {
						}

						@Override
						public void onTabSelected(Tab selectedTab,
								FragmentTransaction arg1) {
							mViewPager.setCurrentItem(selectedTab.getPosition());
						}

						@Override
						public void onTabReselected(Tab arg0,
								FragmentTransaction arg1) {
						}
					}));

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Intent intent = new Intent(this, SubContentActivity.class);
		intent.putExtra(SubContentActivity.DATA,
				(Serializable) mainContents.get(position));

		startActivity(intent);

	}

	@Override
	public void onLoaderFinish(List<Content> contents) {

		this.mainContents = contents;

	}

	@Override
	public void onPlayButtonClick(View v, Content content, int position) {
		if (this.position != position) {
			stopAudio();
			playAudio(v, content, position);
		} else {
			if (content.get_play_icon() == R.drawable.ic_play)
				playAudio(v, content, position);
			else
				stopAudio();
		}
	}

	private void stopAudio() {
		// currentPlayButton.setImageResource(R.drawable.ic_play);
		if (currentMedia != null && currentContent != null) {
			currentMedia.stop();
			onCompletion(currentMedia);
		}
	}

	private void playAudio(View v, Content content, int position) {
		int result;

		this.currentContent = content;
		this.position = position;

		result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);

		if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {

			try {
				AssetFileDescriptor afd;
				afd = getAssets().openFd(content.getVoice() + ".mp3");
				currentMedia.reset();
				currentMedia.setDataSource(afd.getFileDescriptor(),
						afd.getStartOffset(), afd.getLength());
				currentMedia.prepare();
				currentMedia.setOnCompletionListener(this);
				currentMedia.start();
				ImageButton currentPlayButton = (ImageButton) v;
				content.set_play_icon(R.drawable.ic_stop);
				currentPlayButton.setImageResource(content.get_play_icon());
			} catch (IOException e) {
				Log.e(LOG_TAG, e.getMessage());
			}

			/*
			 * currentMedia = MediaPlayer.create(this, R.raw.morning);
			 * currentMedia.setOnCompletionListener(this); currentMedia.start();
			 */

		}
	}

	@Override
	public void onDeleteButtonClick(View v, Content content, int position) {
		
		long rowAffected = _app.getBookmarkDAO().unBookmark(content.getID());
		if (rowAffected >0) {
			content.set_is_bookmark(false);
			BookMarkFragment bookmarkFragment = (BookMarkFragment) contentPagerAdapter.getFragment(1);
			bookmarkFragment.getBookmarkAdapter().remove(content);
			bookmarkFragment.getBookmarkAdapter().notifyDataSetChanged();
		}

	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		currentContent.set_play_icon(R.drawable.ic_play);
		// currentPlayButton.setImageResource(currentContent.get_play_icon());
		
		//String bookmarkFragmentTag= ContentPagerAdapter.makeFragmentName(mViewPager.getId(), 1);
		
		//BookMarkFragment bookmarkFragment = (BookMarkFragment) getSupportFragmentManager().findFragmentByTag(bookmarkFragmentTag);
		
		BookMarkFragment bookmarkFragment = (BookMarkFragment) contentPagerAdapter.getFragment(1);
		if (bookmarkFragment != null) {
			View listItem = bookmarkFragment.subContentList.getChildAt(position - bookmarkFragment.subContentList.getFirstVisiblePosition());
			if (listItem != null) {
				ImageButton playButton = (ImageButton) listItem.findViewById(R.id.playIcon);
				playButton.setImageResource(currentContent.get_play_icon());
			}
		}

		// mp.release();
		// currentMedia = mp = null;
		// currentPlayButton = null;

		am.abandonAudioFocus(this);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		outState.putSerializable(CURRENT_CONTENT, currentContent);
		outState.putInt(CURRENT_POSITION, position);

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
		currentMedia.release();
		currentMedia = null;
		super.onBackPressed();
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		MediaPlayer mediaPlayer = currentMedia;
		// Clear our member variable to guarantee this Activity
		// is allowed to GC after onDestroy()
		currentMedia = null;
		return mediaPlayer;
	}
}
