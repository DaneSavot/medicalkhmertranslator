package com.dane.medicalkhmertranslator;

import android.view.View;

import com.dane.medicalkhmertranslator.db.model.Content;

public interface BookmarkFragmentEventHandler {
	public void onPlayButtonClick(View v, Content content, int position);
	public void onDeleteButtonClick(View v, Content content, int position);
}
